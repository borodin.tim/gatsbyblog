import React from 'react'
import Img from 'gatsby-image'

import Layout from "../components/layout"
import Button from "../components/button"

export default function Template({ data }) {
    const post = data.markdownRemark

    return (
        <Layout>
            <h2 className="post-title">{post.frontmatter.title}</h2>
            <small>Posted by {post.frontmatter.author} on {post.frontmatter.date}</small>
            <br />
            <Img className="post-featured-image"
                fluid={post.frontmatter.featuredImage.childImageSharp.fluid}
                alt={post.frontmatter.title}
            />
            {console.log('__html: post.html.replace(/\n+$/, ""):', post.html.replace(/\n+$/, ""))}
            <div className="post" dangerouslySetInnerHTML={{ __html: post.html.replace(/\n+$/, "") }}></div>
            <Button label={'Go back'} to={"/blog"} />
        </Layout>
    )
}

export const postQuery = graphql`
    query BlogPostByPath($path: String!){
        markdownRemark(frontmatter: { path: { eq: $path }}){
            html
            frontmatter{ 
                title
                path
                date
                author
                featuredImage {
                    childImageSharp {
                        fluid(maxWidth: 800) {
                            ...GatsbyImageSharpFluid
                        }
                    }
                }
            }
        }
    }
`


// <Link to="/blog">
// <div className="link-btn">
//     Go back
// </div>
// </Link>