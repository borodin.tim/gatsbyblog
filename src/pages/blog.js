import React from "react"
import { Link } from "gatsby"
import Img from 'gatsby-image'

import Layout from "../components/layout"
import SEO from "../components/seo"
import Button from "../components/button"

const BlogPage = ({ data }) => (
  <Layout>
    <SEO title="BlogPage" />
    {
      data.allMarkdownRemark.edges
        .sort((a, b) => {
          const aDate = new Date(a.node.frontmatter.date);
          const bDate = new Date(b.node.frontmatter.date);
          return bDate - aDate;
        })
        .map(post => (
          <div key={post.node.id}>
            <h2 className="post-title">
              <Link to={post.node.frontmatter.path}>
                {post.node.frontmatter.title}
              </Link>
            </h2>
            <div className="post-contents">
              <small>Posted by {post.node.frontmatter.author} on {post.node.frontmatter.date}</small>
              <br />
              {/*console.log('======================================')*/}
              {/*console.log('post.node.html:', post.node.html.substring(0, 100))*/}
              {/*console.log('======================================')*/}

              { /* More on Img - https://www.gatsbyjs.com/docs/working-with-images-in-markdown/?ref=hackernoon.com */}
              <Link to={post.node.frontmatter.path}>
                <Img className="post-featured-image"
                  fluid={post.node.frontmatter.featuredImage.childImageSharp.fluid}
                  alt={post.node.frontmatter.title}
                />
              </Link>
              <div className="post-preview" dangerouslySetInnerHTML=
                {{
                  __html: post.node.html.substring(0, 300)
                }}
              ></div>
              <Button label={'Read more'} to={post.node.frontmatter.path} />
              <br /><br /><br />
            </div>
          </div>
        ))}
  </Layout >
)

export const pageQuery = graphql`
query BlogIndexQuery{
  allMarkdownRemark{
    edges{
      node{    
        id
        html
        frontmatter {
          title
          path
          date
          author
          featuredImage {
            childImageSharp {
              fluid(maxWidth: 800) {
                ...GatsbyImageSharpFluid
              }
            }
          }
        }
      }
    }
  }
}
`

export default BlogPage
