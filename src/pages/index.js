import React from "react";
import { Link } from "gatsby";

import Layout from "../components/layout"
import SEO from "../components/seo"
import Button from "../components/button"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h2>Hi everyone</h2>
    <p>Welcome to my Blog built with Gatsby.</p>
    {/* <Button label={'Read Blog posts'} to={"/blog"} className="read-blog-btn" /> */}
    <Link to={'/blog'}>
      <div className="link-btn read-blog-btn">
        Read Blog posts
      </div>
    </Link>
    <br />
  </Layout>
)

export default IndexPage
