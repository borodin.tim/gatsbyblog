---
path: "/blog/post-5"
date: "02/11/2021"
title: "Firefox sorting by date issue"
author: "Tim Borodin"
featuredImage: "image.jpeg"
---

Just found this weird behavior of different browsers...  
Had problems sorting posts by date in Firefox, but Chrome worked alright.  
Apparently firefox didn't understand dates in format 11-02-2021, but needed 11/02/2021.
