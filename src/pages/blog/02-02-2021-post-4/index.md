---
path: "/blog/post-4"
date: "02/02/2021"
title: "This is the 4th and the longes post that I've made"
author: "Tim Borodin"
featuredImage: "image.jpeg"
---

This is my Fourth blog post.  
Added some About me data, and sorting by created date. Newest posts at the top.  
Blow is the code that executes sorting:

        data.allMarkdownRemark.edges
        .sort((a, b) => {
                const aDate = new Date(a.node.frontmatter.date);
                const bDate = new Date(b.node.frontmatter.date);
                return bDate - aDate;
        })

The data is initialy saved as string hence converting to Data is necessary.  
Also added some info into About section.
