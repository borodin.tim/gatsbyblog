import React from 'react'

import Layout from "../components/layout"
import SEO from "../components/seo"

const AboutPage = () => (
    <Layout>
        <SEO title="About" />
        <h2>Tim Borodin</h2>
        <p>
            My name's Tim and I'm a Web Developer.
            <br /><br />
            Former QA Automation Engineer at IBM strongly motivated to become a Full-Stack Engineer. I'm looking for an opportunity to expand my knowledge and expertise on a Mid-Level developer position. I believe that my skills can be valuable in various IT fields and projects.
            <br /><br />
            My speciality is MERN stack - MongoDB, Express, React, NodeJS
        </p>
    </Layout>
)

export default AboutPage;