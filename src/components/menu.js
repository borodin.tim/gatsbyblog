import React from 'react'

import { Link } from 'gatsby';

const Menu = () => (
    <nav>
        <ul role="menubar" aria-label="menubar1">
            <li role="menuitem" aria-label="Home" >
                <Link to="/" title="Home" activeClassName="selected">Home</Link>
            </li>
            <li role="menuitem" aria-label="Blog">
                <Link to="/blog" title="Blog" activeClassName="selected" partiallyActive={true}>Blog</Link>
            </li>
            <li role="menuitem" aria-label="About">
                <Link to="/about" title="About" activeClassName="selected">About</Link>
            </li>
        </ul>
    </nav>
)

export default Menu;

// className="selected" 