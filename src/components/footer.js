import React from "react"
import fontawesome from '@fortawesome/fontawesome'
import fa from "@fortawesome/fontawesome-free";
import fab from "@fortawesome/fontawesome-free-brands";
import fas from "@fortawesome/fontawesome-free-solid";

//https://fontawesome.com/v5.0.13/how-to-use/with-the-api/setup/importing-icons

fontawesome.library.add(fa, fab, fas);

const Footer = () => (
  <footer>
    <br />
    <div className="footer-container">
      <div>© Copyright  {new Date().getFullYear()} All rights reserved</div>
      <div className="social-networks">
        <a href="https://gitlab.com/borodin.tim" target="_blank" rel="noreferrer noopener nofollow" title="gitlab">
          <i className="fab fa-gitlab"></i>
        </a>
        <a href="https://www.linkedin.com/in/timurborodin/" target="_blank" rel="noreferrer noopener nofollow" title="linkedin">
          <i className="fab fa-linkedin"></i>
        </a>
        <a href="https://github.com/De1ain" target="_blank" rel="noreferrer noopener nofollow" title="github">
          <i className="fab fa-github-square"></i>
        </a>
        <a href="mailto:borodin.tim@gmail.com?subject=My Blog Built with Gatsby" target="_blank" rel="noreferrer noopener nofollow" title="email">
          <i className="fas fa-envelope-square"></i>
        </a>
      </div>
      <div>Created by Tim Borodin</div>
    </div>
    <br />
  </footer>
)

export default Footer
