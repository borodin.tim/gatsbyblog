import React from "react"
import { Link } from "gatsby"

const Button = (props) => (
    <Link to={props.to}>
        <div className="link-btn">
            {props.label}
        </div>
    </Link>
)

export default Button
